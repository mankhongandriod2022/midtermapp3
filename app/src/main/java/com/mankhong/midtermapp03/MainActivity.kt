package com.mankhong.midtermapp03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mankhong.midtermapp03.Model.Oil
import com.mankhong.midtermapp03.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        val oilList: List<Oil> = listOf(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 37.24),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 38.08),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 38.35),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.84),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )
        val recyclerView = findViewById<RecyclerView>(R.id.RecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(oilList)
        var adapters = ItemAdapter(oilList)
        recyclerView.adapter = adapters
        adapters.setOnItemClickListener(object : ItemAdapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                Toast.makeText(
                    this@MainActivity,
                    oilList[position].price.toString() + "     " + oilList[position].name,
                    Toast.LENGTH_LONG
                ).show()
            }
        })




    }
    class ItemAdapter(val items: List<Oil>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

        private lateinit var mListener: onItemClickListener

        interface  onItemClickListener{
            fun onItemClick(position: Int)
        }

        fun setOnItemClickListener(listener: onItemClickListener) {
            mListener = listener
        }

        class ViewHolder(private val itemView: View,listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){
            val price = itemView.findViewById<TextView>(R.id.Price)
            val name = itemView.findViewById<TextView>(R.id.Name)
            init {
                itemView.setOnClickListener{
                    listener.onItemClick(adapterPosition)
                }
            }
        }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.list,parent,false)
            return ViewHolder(adapterLayout,mListener)
        }

        override fun onBindViewHolder(holder: ItemAdapter.ViewHolder, position: Int) {
            holder.name.text = items[position].name
            holder.price.text = items[position].price.toString()
        }

        override fun getItemCount(): Int {
            return items.size
        }
    }


}