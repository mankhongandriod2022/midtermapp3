package com.mankhong.midtermapp03.Model

data class Oil (val name: String, val price: Double)